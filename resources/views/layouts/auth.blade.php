<?php
/**
 * Created by PhpStorm.
 * User: andromeda
 * Date: 27/01/18
 * Time: 11:24
 */
?>
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('head_title')</title>

    {{--Styles--}}
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
    <style>
        .center {
            margin: auto;
        }
        .blank {
            margin: 0 0 2em 0;
        }
        .page-header {
            font-weight: bold !important;
        }
        .col-md-7{
            padding: 0 0 15px 0;
        }
        .left {
            float: left;
        }
        .right {
            float: right;
        }
    </style>
</head>
<body>
<div id="app" class="text-center" style="padding: 2em;">
    <nav class="navbar navbar-light">
        <div class="container">
            <a href="./" class="navbar-brand" style="margin: auto;">
                Ini Logo
            </a>
        </div>
    </nav>

    @yield('content')
</div>
</body>
</html>
