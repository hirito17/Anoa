@extends('layouts.auth')
@section('head_title') AnoaClothes - Daftar @endsection

@section('content')
<div class="container" style="padding: 2em 0;">
    <div class="page-header blank">
        Daftar Akun Baru Sekarang
    </div>
    <div class="row">
        <div class="col-md-8" style="margin: auto">
            <form method="POST" class="form-horizontal" action="{{ route('register') }}">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <div class="col-md-7 center">
                        <input type="text" id="name" class="form-control"
                               name="name" value="{{ old('name') }}"
                               placeholder="Nama Lengkap" required autofocus>
                        @if ($errors->has('name'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <div class="col-md-7 center">
                        <input type="email" id="email" class="form-control"
                               name="email" value="{{ old('email') }}"
                               placeholder="Email" required autofocus>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                    <div class="col-md-7 center">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"
                                      id="basic-addon3">http://anoaclothes.com/</span>
                            </div>
                            <input type="text" id="username" class="form-control"
                                   name="username" value="{{ old('username') }}"
                                   placeholder="Username" required>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                    <div class="col-md-7 center">
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="male" name="gender"
                                   class="custom-control-input" value="male">
                            <label class="custom-control-label" for="male">Laki-laki</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="female" name="gender"
                                   class="custom-control-input" value="female">
                            <label class="custom-control-label" for="female">Perempuan</label>
                        </div>
                        @if ($errors->has('gender'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('gender') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <div class="col-md-7 center">
                        <input type="password" id="password" class="form-control"
                               name="password" placeholder="Password" required>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-7 center">
                        <input type="password" id="password_confirmation" class="form-control"
                               name="password_conofirmation" placeholder="Konfirmasi Password" required>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-7 center">
                        <span>
                            Dengan klik daftar, kamu telah menyetujui <a href="#" class="text-primary">Aturan Penggunaan</a> dan <a href="#" class="text-primary">Kebijakan Privasi</a> dari AnoaClothes
                        </span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-7 center">
                        <button type="submit" class="btn btn-block btn-primary">Daftar</button>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-7 center">
                        <p class="font-weight-light">
                            Sudah punya akun? <a href="{{ route('login') }}" class="text-primary">Silahkan Login</a>
                        </p>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection