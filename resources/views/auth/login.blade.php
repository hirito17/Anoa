@extends('layouts.auth')
@section('head_title') AnoaClothes - Login @endsection

@section('content')
    <div class="container" style="padding: 2em 0;">
        <div class="page-header blank">
            Silahkan masuk
        </div>
        <div class="row">
            <div class="col-md-8" style="margin: auto">
                <form action="{{ route('login') }}" class="form-horizontal" method="POST">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <div class="col-md-7 center">
                            <input type="text" id="email" class="form-control"
                                   name="email" value="{{ old('email') }}"
                                   placeholder="Email atau Username" required autofocus>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <div class="col-md-7 center">
                            <input type="password" id="password" class="form-control"
                                   name="password" placeholder="Password" required>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-7 center">
                            <div class="custom-control custom-checkbox col-md-4">
                                <input type="checkbox" id="remember" name="remember"
                                       class="custom-control-input" {{ old('remember') ? 'checked' : '' }}>
                                <label class="custom-control-label float-left" for="remember">Ingat saya</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-7 center">
                            <button type="submit" class="btn btn-block btn-primary">Login</button>
                        </div>
                    </div>
                    <div class="form-group" style="margin: 0;">
                        <div class="col-md-4 center">
                            <p class="font-weight-light">
                                <a href="{{ route('password.request') }}" class="text-primary">Lupa Password?</a>
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 center">
                            <p class="font-weight-light">
                                Belum punya akun? <a href="{{ route('register') }}" class="text-primary">Daftar disini</a>
                            </p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection