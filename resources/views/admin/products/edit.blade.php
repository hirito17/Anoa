<?php
/**
 * Created by PhpStorm.
 * User: andromeda
 * Date: 05/02/18
 * Time: 20:17
 */
?>
@extends('layouts.admin.master')

@section('css')
    .col-form-label {
    font-size: 15px;
    }
    .red{
    color: #d34055;
    }
@endsection

@section('content')

    <main class="main-container">
        <div class="main-content">

            <div class="header-info">
                <div class="left">
                    <h2 class="header-title"><strong>Create</strong> Product</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.product') }}">Product</a></li>
                        <li class="breadcrumb-item active">Edit</li>
                    </ol>
                </div>
            </div>

            <form action="{{ route('admin.product.update', $product->slug) }}" method="POST"
                  class="form-horizontal" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <div class="row">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-title">
                                <h3><strong>Edit</strong> Barang</h3>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="name" class="col-form-label">Name <span class="red">*</span></label>
                                    <input id="name" type="text" class="form-control" name="name"
                                           placeholder="Nama Barang" value="{{ $product->name }}" required>
                                </div>
                                <div class="form-group">
                                    <label for="categories" class="col-form-label">Kategori Barang <span
                                                class="red">*</span></label>
                                    <select id="categories.0" name="categories[]" data-provide="selectpicker"
                                            class="form-control" multiple
                                            data-max-options="2" required>
                                        @foreach($categories as $value)
                                            <option value="{{ $value->id }}">{{ $value->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-title">
                                <h3><strong>Detail</strong> Barang</h3>
                            </div>
                            <div class="card-body">
                                @foreach($attr as $key => $value)
                                    <div class="form-group">
                                        <label for="attr.0" class="col-form-label">Harga Barang <span
                                                    class="red">*</span></label>
                                        <input id="attr.0" type="number" class="form-control" name="attr[price]"
                                               placeholder="Harga Barang" value="{{ $value->price }}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="attr.1" class="col-form-label">Perkiraan Berat <span
                                                    class="red">*</span></label>
                                        <input id="attr.1" type="number" class="form-control" name="attr[weight]"
                                               placeholder="Perkiraan Berat (dalam gram)" value="{{ $value->weight }}"
                                               required>
                                    </div>
                                    <div class="form-group">
                                        <label for="attr.2" class="col-form-label">Stok <span
                                                    class="red">*</span></label>
                                        <input id="attr.2" type="number" class="form-control" name="attr[stock]"
                                               placeholder="Stok barang" value="{{ $value->stock }}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="attr.3" class="col-form-label">Kondisi Barang <span
                                                    class="red">*</span></label>
                                        <br>
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="radio" name="attr[condition]"
                                                       id="attr.3" value="new"
                                                        {{ $value->condition ==  'new' ? 'checked' : ''}} > Baru
                                            </label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="radio" name="attr[condition]"
                                                       id="attr.3" value="second"
                                                        {{ $value->condition ==  'second' ? 'checked' : ''}} > Bekas
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="attr.4" class="col-form-label">Ukuran <span
                                                    class="red">*</span></label>
                                        <input id="attr.4" type="text" class="form-control" name="attr[size]"
                                               placeholder="Ukuran barang (Harap isi dengan benar)"
                                               value="{{ $value->size }}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="attr.5" class="col-form-label">Deskripsi Barang <span
                                                    class="red">*</span></label>
                                        <textarea id="attr.5" class="form-control" name="attr[description]"
                                                  placeholder="Deskripsi" rows="6"
                                                  required>{{ $value->description }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-label btn-primary">
                                            <label><i class="ti-check"></i></label> Tambah
                                        </button>
                                        <a href="{{ route('admin.product') }}" data-provide="animsition">
                                            <button class="btn btn-label btn-warning">
                                                <label><i class="ti-arrow-left"></i></label> Batal
                                            </button>
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </main>

@endsection