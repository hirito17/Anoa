<?php
/**
 * Created by PhpStorm.
 * User: andromeda
 * Date: 01/02/18
 * Time: 20:23
 */
?>
@extends('layouts.admin.master')

@section('content')

    <main class="main-container">
        <div class="main-content">
            <header class="header">
                <div class="header-info">
                    <div class="left">
                        <h2 class="header-title">
                            <strong>{{ $product->name }}</strong></h2>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('admin.product') }}">Product</a></li>
                            <li class="breadcrumb-item">Image</li>
                        </ol>
                    </div>
                </div>
            </header>
            <div class="row">
                @foreach($product->images as $value)
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-title">{{ $value->slug }}</div>
                            <div class="card-body">
                                <img src="{{ route('media.proxy', $value->src) }}" alt="{{ $value->slug }}">
                                <table style="margin: auto;">
                                    <td>
                                        <form method="post"
                                              onclick="return confirm('Apakah anda yakin ' +
                                       'ingin menghapus gambar ini')"
                                              action="{{ route('admin.product.image.destroy',
                                              [$product->slug, $value->slug]) }}">
                                            {!! csrf_field() !!}
                                            <button class="btn btn-sm btn-label btn-danger">
                                                <label><i class="ti-trash"></i></label> Hapus
                                            </button>
                                        </form>
                                    </td>
                                </table>
                            </div>
                        </div>
                    </div>
                @endforeach
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-title">
                            <center><strong>Tambah Gambar</strong></center>
                        </div>
                        <div class="card-body" style="margin: auto;">
                            <form action="{{ route('admin.product.image.store', $product->slug) }}" method="post" enctype="multipart/form-data">
                                {!! csrf_field() !!}
                                <input id="images.0" type="file" data-provide="dropify" name="images[]" multiple required>
                                <center><div class="fab" style="margin-top: 10px;">
                                    <button class="btn btn-float btn-primary"><i class="ti-plus"></i></button>
                                </div></center>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

@endsection
