<?php

use App\Entities\Accounts\Address;
use Illuminate\Database\Seeder;

class AddressesTableSeeder extends Seeder
{
    protected $data = [
        [
            'name' => 'Test',
            'address' => 'Jl. Embong wungu',
            'state' => 'Jawa timur',
            'district' => 'Kali asin',
            'subdistrict' => 'gak ero',
            'phone' => 123456789,
            'type' => ['off'],
            'user_id' => 1
        ], [
            'name' => 'Sapi',
            'address' => 'Jl. Embong ',
            'state' => 'Jawa timur',
            'district' => 'Kali ',
            'subdistrict' => 'gak',
            'phone' => 345678,
            'type' => ['pengirim', 'penagih'],
            'user_id' => 2
        ], [
            'name' => 'Test',
            'address' => 'Jl. Nginden',
            'state' => 'Jawa timur',
            'district' => 'Nginden jangkungan',
            'subdistrict' => 'Wes talah',
            'phone' => 657483920,
            'type' => ['pengirim, penagih'],
            'user_id' => 1
        ],
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data as $datum) {
            Address::create($datum);
        }
    }
}
