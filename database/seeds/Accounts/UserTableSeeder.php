<?php

use App\Entities\Accounts\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    protected $data = [
        [
            'name' => 'test',
            'email' => 'test@mail.com',
            'username' => 'admin',
            'password' => 'password',
            'gender' => 'male',
            'birthday' => 18112000,
            'role' => 'admin'
        ],[
            'name' => 'lagi',
            'email' => 'lagi@mail.com',
            'username' => 'lagi',
            'password' => 'password',
            'gender' => 'male',
            'birthday' => 18112000,
            'role' => 'member'
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data as $datum) {
            User::create($datum);
        }
    }
}
