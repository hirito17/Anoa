<?php

namespace App\Jobs\Categories;

use App\Core\Patch\JobPatcher;
use App\Entities\Categories\Category;
use App\Entities\Products\Product;
use Illuminate\Http\Request;

class AttachCategory extends JobPatcher
{
    protected $validateRules = [
        'categories' => 'required|array',
    ];

    protected $product;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Request $request, Product $product)
    {
        parent::__construct($request);
        $this->product = $product;
    }

    public function run()
    {
        foreach ($this->request->input('categories') as $item) {
            $this->product->categories()
                ->sync($item);
        }
    }
}
