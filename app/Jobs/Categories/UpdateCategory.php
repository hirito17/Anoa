<?php

namespace App\Jobs\Categories;

use App\Core\Patch\JobPatcher;
use App\Entities\Products\Product;
use Illuminate\Http\Request;

class UpdateCategory extends JobPatcher
{
    protected $validateRules = [
        'categories' => 'required|array',
    ];

    protected $product;

    /**
     * Create a new job instance.
     *
     * @param Request $request
     * @param Product $product
     */
    public function __construct(Request $request, Product $product)
    {
        parent::__construct($request);
        $this->product = $product;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->request->input('categories') as $item) {
            $this->product->categories()
                ->sync($item);
        }

        return $this->product;
    }
}
