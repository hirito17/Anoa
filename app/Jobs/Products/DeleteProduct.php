<?php

namespace App\Jobs\Products;

use App\Core\Patch\JobPatcher;
use App\Entities\Products\Product;
use App\Jobs\Categories\DeleteCategories;

class DeleteProduct extends JobPatcher
{


    protected $product;

    protected $id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Product $product)
    {
        $this->product = $product;
        $this->id = $this->product->id;
        dd($this->product->categories()->count());
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function run()
    {
        $attr = $this->product->attr()->where(
            'product_id', '=', $this->id
        );
        $attr->delete();

        $this->product->delete();

        return $this->product;
    }

    public function callback()
    {
        if ($this->product->categories()->count() > 0)
            dispatch(new DeleteCategories($this->product, $this->id));
        if ($this->product->images()->count() > 0)
            dispatch(new DeleteCategories($this->product, $this->id));
    }
}
