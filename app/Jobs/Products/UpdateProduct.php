<?php

namespace App\Jobs\Products;

use App\Core\Patch\JobPatcher;
use App\Entities\Products\Product;
use App\Jobs\Categories\UpdateCategory;
use Illuminate\Http\Request;

class UpdateProduct extends JobPatcher
{
    protected $validateRules = [
        'name' => 'required',
        'attr.*' => 'required'
    ];

    protected $product;

    protected $attr;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Request $request, Product $product)
    {
        parent::__construct($request);
        $this->product = $product;
        $this->attr = $this->request->input('attr');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function run()
    {
        $this->product->update([
            'name' => $this->request->name
        ]);

        $attr = $this->product->attr()->where('product_id',
            '=', $this->product->id);
        $attr->update([
            'price' => $this->attr['price'],
            'weight' => $this->attr['weight'],
            'stock' => $this->attr['stock'],
            'condition' => $this->attr['condition'],
            'size' => $this->attr['size'],
            'description' => $this->attr['description']
        ]);
    }

    public function callback()
    {
        if ($this->request->has('categories'))
            dispatch(new UpdateCategory($this->request, $this->product));
    }
}
