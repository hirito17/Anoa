<?php

namespace App\Http\Controllers;

use Illuminate\Filesystem\Filesystem;

class MediaController extends Controller
{
    public function file($src)
    {
        $filesystem = app()->make(Filesystem::class);

        $file = $filesystem->get(storage_path('app/'. $src));

        if (request()->input('download', false)) {
            return response()->download(storage_path('app/'. $src), $filesystem->name($path));
        }

        $mime = $filesystem->mimeType(storage_path('app/'. $src));

        $response = response()->make($file, 200);

        $response->header('Content-Type',$mime);

        return $response;
    }
}
