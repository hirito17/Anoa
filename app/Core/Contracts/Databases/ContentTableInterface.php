<?php
/**
 * Created by PhpStorm.
 * User: andromeda
 * Date: 01/02/18
 * Time: 16:26
 */
namespace App\Core\Contracts\Databases;

interface ContentTableInterface {

    /**
     * Get all of the images for the products and users
     *
     * @return Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function images();

}