<?php

namespace App\Entities;

use App\Entities\Accounts\User;
use App\Entities\Products\Product;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = 'images';

    protected $fillable = [
        'src', 'slug'
    ];

    public function products()
    {
        return $this->morphedByMany(Product::class, 'imageable');
    }

    public function users()
    {
        return $this->morphedByMany(User::class, 'imageable');
    }

    public function scopeBySlug($query, $value)
    {
        return $query->where('slug', '=', $value)->first();
    }
}
