<?php

namespace App\Entities\Products;

use App\Entities\Accounts\User;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $table = 'carts';

    protected $fillable = [
        'user_id', 'product_id', 'mount'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id',
            'id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id',
            'id');
    }
}
