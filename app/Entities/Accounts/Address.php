<?php

namespace App\Entities\Accounts;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $table = 'addresses';

    protected $fillable = [
        'name', 'address', 'state', 'district', 'subdistrict', 'phone'
    ];

    protected $casts = [
        'type' => 'json'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
