<?php

namespace App\Entities\Accounts;

use App\Core\Contracts\Databases\ContentTableInterface;
use App\Entities\Image;
use App\Entities\Products\Cart;
use App\Entities\Products\Discussion;
use App\Entities\Products\Product;
use App\Entities\Products\Review;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements ContentTableInterface
{
    use Notifiable;

    const ADMIN = "admin";
    const MEMBER = "member";

    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'username', 'password', 'gender', 'birthday','role', 'ban'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'ban' => 'boolean'
    ];

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    public function addresses()
    {
        return $this->hasMany(Address::class, 'user_id', 'id');
    }

    public function carts()
    {
        return $this->hasMany(Cart::class, 'user_id', 'id');
    }

    public function discuss()
    {
        return $this->hasMany(Discussion::class, 'user_id',
            'id');
    }

    public function discussReps()
    {
        return $this->hasMany(Discussion::class, 'user_id',
            'id');
    }

    public function reviews()
    {
        return $this->hasMany(Review::class, 'user_id',
            'id');
    }

    public function posts()
    {
        return $this->hasMany(Product::class, 'user_id',
            'id');
    }

    public function favorites()
    {
        return $this->belongsToMany(Product::class, 'favorites',
            'user_id', 'product_id');
    }

    public function images()
    {
        return $this->morphToMany(Image::class, 'imageable');
    }

    public function scopeMember($query)
    {
        return $query->where('role', static::MEMBER);
    }

    public function isMember()
    {
        return $this->attributes['role'] == self::MEMBER ? true : false;
    }

    public function isAdmin()
    {
        return $this->attributes['role'] == self::ADMIN ? true : false;
    }

}
